<?php

return [

    'AgregarAlumno' => 'Añadir Alumno',
    'Alumno' => 'Alumno',
    'Nombre'  		=> 'Nombre: ',
    'Titulo'		=> 'Titulo: ',
    'Reporte'		=> 'Reporte: ',
    'TituloIndex'		=> 'Titulo',
    'ReporteIndex'		=> 'Reporte',
    'ModificarAlumno'=> 'Modificar el Registro',
    'Eliminar'=>'Borrar Registro del Alumno',
    'EditarRegistro' => 'Editar Registro del Alumno: ',
    'SistemaReportes' => 'Sistema de Reportes para estudiantes',
    'AlumnosLista' => 'Lista de Alumnos',
    'Acciones' => 'Acciones',
    'InicioNav' => 'Inicio',
    'Editar' => 'Editar',
    'Mostrar' => 'Mostrar',
    'AlumnoInfo' => 'Información del Alumno',
    'Cancelar' => 'Cancelar',
];
