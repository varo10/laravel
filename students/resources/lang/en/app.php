<?php

return [

    'AgregarAlumno' => 'Add Student',
    'Alumno' => 'Student',
    'Nombre'  		=> 'Name: ',
    'Titulo'		=> 'Title: ',
    'Reporte'		=> 'Report: ',
    'TituloIndex'		=> 'Title',
    'ReporteIndex'		=> 'Report',
    'ModificarAlumno'=> 'Modify Registry',
    'Eliminar'=>'Delete Student Registry',
    'EditarRegistro' => 'Edit Registry of the Student: ',
    'SistemaReportes' => 'Reports System for students',
    'AlumnosLista' => 'Students List',
    'Acciones' => 'Actions',
    'InicioNav' => 'Home',
    'Editar' => 'Edit',
    'Mostrar' => 'Show Details',
    'AlumnoInfo' => 'Student Information',
    'Cancelar' => 'Cancel',
];
