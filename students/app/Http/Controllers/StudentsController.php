<?php

namespace App\Http\Controllers;

use Request;

use App\Http\Requests;
use App\Http\Requests\CreateAlumnoRequest;
use App\Http\Controllers\Controller;
use App\Alumnos;

class StudentsController extends Controller
{
    public function index()
    {
    	$students = Alumnos::all();
    	return view('students.index' , compact('students'));
    }

    public function show($id)
    {
    	$students = Alumnos::find($id);
    	return view('students.show' , compact('students'));
    }

    public function create()
    {
    	return view('students.create');
    }

    public function store(CreateAlumnoRequest $request)
    {
    	Alumnos::create($request->all());
    	return redirect('students');
    }

    public function edit($id)
    {
    	$student = Alumnos::find($id);
    	return view('students.edit', compact('student'));
    }

    public function update($id, CreateAlumnoRequest $request)
    {
    	$student = Alumnos::find($id);
    	$student->update($request->all());

    	return redirect('students');
    }

    public function destroy($id)
    {
    	$student = Alumnos::find($id);
    	$student->delete();

    	return redirect('students');
    }
}
