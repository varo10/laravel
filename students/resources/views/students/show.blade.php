@extends ('master')

@section ('contenido')
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">		<div class="navbar-header">
			 <ul class="nav navbar-nav">
		        <li class="active"><a href="/students"> {{trans('app.InicioNav')}} </a></li>
		      </ul>
		</div>
	</div>
</nav>

<div class="container container-fluid" >
	<h1> {{trans('app.AlumnoInfo')}} </h1>
	<h2> {{trans('app.Titulo')}} {{ $students->titulo}}</h2>
	<h2> {{trans('app.Nombre')}} {{ $students->alumno}} </h2>
	<h2> {{trans('app.Reporte')}} {{ $students->reporte}}</h2>	
</div>

<footer class="container-fluid text-center">
  <p> <span class="glyphicon glyphicon-copyright-mark"></span> Alvaro Padilla Mendoza  </p>
</footer>
@stop