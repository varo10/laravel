@extends ('master')

@section ('contenido')
<div class="jumbotron">
	<div class="container-fluid">
		<div >
			<h2> {{trans('app.SistemaReportes')}} </h2>
		</div>
	</div>
</div>
<br>
<div class="panel panel-default">
	<div class="panel-heading"> {{trans('app.AlumnosLista')}} </div>
	<table class="table table-responsive table-bordered">
		<thead>
			<tr>
				<th> {{trans('app.Alumno')}} </th>
				<th> {{trans('app.TituloIndex')}} </th>
				<th> {{trans('app.ReporteIndex')}} </th>
				<th> {{trans('app.Acciones')}} </th>
			</tr>
		</thead>
		<tbody>
			@foreach ($students as $student)
				<tr>
					<td> {{ $student->alumno}}</td>
					<td> {{ $student->titulo}}</td>
					<td> {{ $student->reporte}}</td>
					<td> 
					<a href="/students/{{$student->id}}/edit" class="btn btn-warning btn-xs"> {{trans('app.Editar')}} </a>
					<a href="/students/{{$student->id}}" class="btn btn-success btn-xs"> {{trans('app.Mostrar')}} </a>
					</td>
				</tr>
			@endforeach
			<tr>
				<th><a href="/students/create" class="btn btn-info btn-sm"> {{trans('app.AgregarAlumno')}}</a></th>
			</tr>
		</tbody>
	</table>
</div>

<footer class="container-fluid text-center">
  <p> <span class="glyphicon glyphicon-copyright-mark"></span> Alvaro Padilla Mendoza  </p>
</footer>

@stop