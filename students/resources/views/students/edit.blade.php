@extends ('master')

@section ('contenido')
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">		<div class="navbar-header">
			 <ul class="nav navbar-nav">
		        <li class="active"><a href="/students"> {{trans('app.InicioNav')}} </a></li>
		      </ul>
		</div>
	</div>
</nav>

<h1> {{trans('app.EditarRegistro')}} {{ $student->alumno }} </h1>
<hr>

{!! Form::model($student , ['method' => 'PATCH', 'action' => ['StudentsController@update' , $student->id] ]) !!}
	<div class="form-group">
		{!! Form::label('alumno' , trans('app.Nombre')) !!}
		{!! Form::text('alumno' , null, ['class' => 'form-control']) !!}	
	</div>
	<div class="form-group">
		{!! Form::label('titulo' , trans('app.Titulo')) !!}
		{!! Form::text('titulo' , null, ['class' => 'form-control']) !!}	
	</div>
	<div class="form-group">
		{!! Form::label('reporte' , trans('app.Reporte')) !!}
		{!! Form::textarea('reporte' , null, ['class' => 'form-control']) !!}	
	</div>
	<div class="form-group">
		{!! Form::submit( trans('app.ModificarAlumno') , ['class' => 'btn btn-primary form-control']) !!}
		
	</div>
	
{!! Form::close() !!}

{!! Form::open(['method' => 'DELETE' , 'route' => ['students.destroy', $student->id] ]) !!}
	<div class="form-group">
		{!! Form::submit( trans('app.Eliminar') , ['class' => 'btn btn-danger form-control']) !!}
	</div>
{!! Form::close() !!}

<div class="form-group">
	<a href="/students/" class="btn btn-default btn-sm"> {{trans('app.Cancelar')}}</a>
</div>

@if ($errors->any())
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li> {{ $error }} </li>
		@endforeach
	</ul>
@endif

<footer class="container-fluid text-center">
  <p> <span class="glyphicon glyphicon-copyright-mark"></span> Alvaro Padilla Mendoza  </p>
</footer>

@stop